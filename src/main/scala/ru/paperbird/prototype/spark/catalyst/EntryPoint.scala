package ru.paperbird.prototype.spark.catalyst

import org.apache.log4j.Logger
import org.apache.spark.sql.catalyst.expressions.AttributeReference
import org.apache.spark.sql.catalyst.plans.logical.{LogicalPlan, Project}
import org.apache.spark.sql.execution.LogicalRDD
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SparkSession}

import scala.collection.mutable


object EntryPoint extends App {

  val logger = Logger.getLogger("ru.paperbird")

  logger.info("EntryPoint -> init")

  val spark = SparkSession
    .builder()
    .appName("spark-catalyst")
    .config("spark.master", "local")
    .getOrCreate()

  /*
  val opts = Map(
    "url" -> "jdbc:postgresql://localhost:5432/postgres?currentSchema=bot-platform-dev",
    "user" -> "bot-platform-dev",
    "password" -> "qwe123"
  )

  val dfUsersJdbc = spark
    .read
    .format("jdbc")
    .options(opts + ("dbtable" -> "\"USERS\""))
    .load
    .as("users")
  dfUsersJdbc.createOrReplaceTempView("users")

  val dfUserGroupsJdbc = spark
    .read
    .format("jdbc")
    .options(opts + ("dbtable" -> "\"USER_GROUPS\""))
    .load
    .as("userGroups")
  dfUserGroupsJdbc.createOrReplaceTempView("userGroups")

  val dfUserToUserGroupsJdbc = spark
    .read
    .format("jdbc")
    .options(opts + ("dbtable" -> "\"USERS_TO_USER_GROUPS\""))
    .load
    .as("usersToUserGroups")
  dfUserToUserGroupsJdbc.createOrReplaceTempView("usersToUserGroups")

  val sqlDF_1 = spark
    .sql("SELECT u.* FROM users AS u " +
      "LEFT OUTER JOIN usersToUserGroups AS u2ug ON u.id = u2ug.user_id " +
      "LEFT OUTER JOIN userGroups AS ug ON ug.id = u2ug.user_group_id "
    )

  val sqlDF_2 = spark
    .sql("SELECT u.login, ug.id FROM users AS u " +
      "LEFT OUTER JOIN usersToUserGroups AS u2ug ON u.id = u2ug.user_id " +
      "LEFT OUTER JOIN userGroups AS ug ON ug.id = u2ug.user_group_id "
    )

  val prDF_3 =
    dfUsersJdbc
      .join(dfUserToUserGroupsJdbc,
        col("users.id") === col("usersToUserGroups.user_id"),
        joinType = "left_outer"
      )
      .join(dfUserGroupsJdbc,
        col("userGroups.id") === col("usersToUserGroups.user_group_id"),
        joinType = "left_outer"
      )
      .select("users.login", "userGroups.id")

      */

  /*
    val usersRDD =
      spark.sparkContext.textFile("src/main/resources/users.txt")
        .map(_.split(","))
        .map(attributes => Row(attributes(0), attributes(1).trim))
  */

  val usersSchema = StructType(
    StructField("id", StringType, nullable = true) ::
      StructField("login", StringType, nullable = true) ::
      StructField("password", StringType, nullable = true) ::
      StructField("is_active", BooleanType, nullable = true) ::
      StructField("service_id", StringType, nullable = true) ::
      StructField("full_name", StringType, nullable = true) ::
      Nil
  )

  val usersToUserGroupsSchema = StructType(
    StructField("user_id", StringType, nullable = true) ::
      StructField("user_group_id", StringType, nullable = true) ::
      Nil
  )

  val userGroupsSchema = StructType(
    StructField("id", StringType, nullable = true) ::
      StructField("tpe", StringType, nullable = true) ::
      Nil
  )

  val prUsersDF_4 = spark
    .createDataFrame(spark.sparkContext.emptyRDD[Row].setName("users1"), usersSchema)
    .as("users1")

  val prUsersToUserGroupsDF_4 = spark
    .createDataFrame(spark.sparkContext.emptyRDD[Row].setName("usersToUserGroups1"), usersToUserGroupsSchema)
    .as("usersToUserGroups1")

  val prUserGroupsDF_4 = spark
    .createDataFrame(spark.sparkContext.emptyRDD[Row].setName("usersGroups1"), userGroupsSchema)
    .as("usersGroups1")

  val prDF_4 =
    prUsersDF_4
      .join(prUsersToUserGroupsDF_4,
        prUsersDF_4("id") === prUsersToUserGroupsDF_4("user_id"),
        joinType = "left_outer"
      )
      .join(prUserGroupsDF_4,
        prUserGroupsDF_4("id") === prUsersToUserGroupsDF_4("user_group_id"),
        joinType = "left_outer"
      )
      .select(prUserGroupsDF_4("id"))

  val prDF_5 =
    prUsersDF_4
      .join(prUsersToUserGroupsDF_4,
        prUsersDF_4("id") === prUsersToUserGroupsDF_4("user_id"),
        joinType = "left_outer"
      )
      .join(prUserGroupsDF_4,
        prUserGroupsDF_4("id") === prUsersToUserGroupsDF_4("user_group_id"),
        joinType = "left_outer"
      )
      .select(prUsersDF_4("login"), prUserGroupsDF_4("id"))

  //  val lp1: LogicalPlan = sqlDF_1.queryExecution.optimizedPlan
  //  val lp2: LogicalPlan = sqlDF_2.queryExecution.optimizedPlan
  //  val lp3: LogicalPlan = prDF_3.queryExecution.optimizedPlan
  val lp4: LogicalPlan = prDF_4.queryExecution.optimizedPlan
  val lp5: LogicalPlan = prDF_5.queryExecution.optimizedPlan

  //  logger.info("\noptimizedPlan 1\n" + lp1.numberedTreeString + "\n\n")
  //  logger.info("\noptimizedPlan 2\n" + lp2.numberedTreeString + "\n\n")
  //  logger.info("\noptimizedPlan 3\n" + lp3.numberedTreeString + "\n\n")
  logger.info("\noptimizedPlan 4\n" + lp4.numberedTreeString + "\n\n")
  logger.info("\noptimizedPlan 5\n" + lp5.numberedTreeString + "\n\n")

  val allFieldsMap = mutable.Map[String, (String, String, Long)]()

  def fold(i: LogicalPlan, path: Seq[String], fm: Map[String, Seq[String]]): Map[String, Seq[String]] = i match {

    case r: LogicalRDD =>

      val table = r.rdd.dependencies.headOption.map(_.rdd.name).getOrElse("_")

      val fields = r.productIterator
        .filter(_.isInstanceOf[Traversable[AttributeReference]])
        .flatMap(i => i.asInstanceOf[Traversable[AttributeReference]])
        .map(i => (table, i.name, i.exprId.id))
        .toSeq

      val modPath = path :+ r.simpleString

      val res = fields.foldLeft(fm) { (acc, field) =>
        val fieldKey = s"${field._2}#${field._3}"
        allFieldsMap += (fieldKey -> field)
        if (acc.isDefinedAt(fieldKey)) acc + (fieldKey -> modPath) else acc
      }

      res

    case r: LogicalPlan =>

      val modPath: Seq[String] = path :+ r.simpleString
      val res = r.children.foldLeft(fm)((a, i) => a ++ fold(i, modPath, a))
      res
  }

  val fields = lp5.asInstanceOf[Project].productIterator
    .collect { case i: Traversable[AttributeReference] => i }
    .flatten
    .map(i => s"${i.name}#${i.exprId.id}")
    .toSeq

  logger.warn(fields.mkString(", "))

  val fieldsMap: Map[String, Seq[String]] = fields.map(f => (f, Seq(f))).toMap

  val res = lp5.children.foldLeft(fieldsMap)((acc, child) => acc ++ fold(child, Seq.empty, acc))

  logger.warn("\n" + res.mkString("\n"))

  logger.warn("\n" + allFieldsMap.mkString("\n"))

  logger.warn("\n" + res.mapValues(v =>
    allFieldsMap.foldLeft(v)((acc, ss) => acc.map(i => i.replaceAll(ss._1, s"${ss._2._1}.${ss._2._2}")))
  ).mkString("\n")
  )

}